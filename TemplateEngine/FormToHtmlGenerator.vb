﻿Imports System.Collections.Specialized
Imports DotLiquid
Imports System.IO
Imports Humanizer
Imports System.Text.RegularExpressions
Imports DotLiquid.Util

''' <summary>
''' A Generator that is capable of taking WebForms Request.Form values, transforming them, and applying them to a Template
''' </summary>
Public Class FormToHtmlGenerator
    Inherits Generator

    Private Const HashUniqueIdKey = "_uniqueId"

    Private Shared _prefixes As IList(Of String)

    ''' <summary>
    ''' Gets the list of prefixes that are stripped from the form keys during processing.
    ''' New prefixes can be added at runtime by any caller using Add().
    ''' </summary>
    Public Shared Property Prefixes As IList(Of String)
        Get
            Return _prefixes
        End Get
        Private Set(value As IList(Of String))
            _prefixes = value
        End Set
    End Property

    Shared Sub New()
        Prefixes = New List(Of String) From {"txt", "rb", "chb", "ddl"}
    End Sub

    Private ReadOnly _templatePath As String
    Private ReadOnly _formValues As NameValueCollection

    ''' <summary>
    ''' Constructs a new FormToHtmlGenerator object
    ''' </summary>
    ''' <param name="templatePath">The path to file containing the template</param>
    ''' <param name="formValues">The NameValueCollection containing the form values</param>    
    Public Sub New(templatePath As String, formValues As NameValueCollection)
        MyBase.New()
        _templatePath = templatePath
        _formValues = formValues
    End Sub

    ''' <summary>
    ''' Parses the template and form values and renders the output
    ''' </summary>
    ''' <returns>The rendered output of the template and form values</returns>    
    Public Overrides Function Build() As String
        Template = File.ReadAllText(_templatePath)
        ParseFormValues()
        Return MyBase.Build()
    End Function

    Private Sub ParseFormValues()
        Values = New Hash
        Dim allKeys = _formValues.AllKeys.ToList()
        For Each key In allKeys
            Dim value = _formValues.Get(key)
            Dim sanitizedKey = SanitizeKey(key)
            If IsMultiDimensionalKey(sanitizedKey) Then
                ParseMultiDimensionalFormValue(sanitizedKey, value)
            Else
                Values.Add(sanitizedKey, value)
            End If
        Next
    End Sub

    Private Shared Function SanitizeKey(key As String) As String
        ' Get the position of the last dollar sign (if any)
        Dim lastDollarSignIndex = key.LastIndexOf("$", StringComparison.CurrentCulture)
        ' If the dollar sign was found, sanitize
        If lastDollarSignIndex > 0 AndAlso key.Length > 1 Then
            key = key.Substring(lastDollarSignIndex + 1)
        End If
        ' Sanitize any prefix from the key
        key = RemovePrefix(key)
        Return key
    End Function

    Private Shared Function RemovePrefix(key As String) As String
        ' Separate the key parts into list. ex.: "txtAddress1" => ["txt", "Address1"]
        Dim keyparts = key.Humanize(LetterCasing.Title).Split(" ").ToList()

        Dim prefix = keyparts.First()
        If Prefixes.Contains(prefix, StringComparison.CurrentCultureIgnoreCase) Then
            keyparts.Remove(prefix)
            key = String.Concat(keyparts)
        End If

        Return key
    End Function

    Private Shared Function IsMultiDimensionalKey(key As String) As Boolean
        ' Returns true if the key contains a number
        Return Regex.IsMatch(key, "\d")
    End Function

    Private Sub ParseMultiDimensionalFormValue(key As String, value As String)
        ' Split key into unique ids. ex.: "Country1City1" => ["Country1", "City1"]
        Dim uniqueIds = Regex.Replace(key, "(\d+)([A-Z])", "$1 $2").Split(" ").ToList()

        ' Split key into collection keys. ex.: "Country1City1" => ["Country", "City"]
        Dim collectionKeys = Regex.Replace(key, "\d+", " ").Split(" ").ToList()
        ' Remove any blank entries. ex.: ["Country", "", "City"] => ["Country", "City"]
        collectionKeys.RemoveAll(AddressOf String.IsNullOrWhiteSpace)

        ' Remove and obtain the last collection key as the current key
        Dim currentKey = collectionKeys.Pop()
        ' Remove and obtain the last uniqueId as the current uniqueId
        Dim currentUniqueId = uniqueIds.Pop()

        ' Pluralize the key names. ex.: ["Country", "City"] => ["Countries", "Cities"]
        collectionKeys = PluralizeKeys(collectionKeys)

        ' Associate each collection key and unique id into collection for iteration
        Dim associations = collectionKeys.Zip(uniqueIds, Function(k, id) New With {Key .Key = k, .UniqueId = id})

        Dim hash = Values
        Dim hashes As List(Of Hash)

        For Each association In associations
            hashes = hash(association.Key)
            If hashes Is Nothing Then
                hashes = New List(Of Hash)
                hash.Add(association.Key, hashes)
            End If

            hash = hashes.FirstOrDefault(Function(h) h(HashUniqueIdKey) = association.UniqueId)
            If hash Is Nothing Then
                hash = New Hash
                hash.Add(HashUniqueIdKey, association.UniqueId)
                hashes.Add(hash)
            End If
        Next

        ' If the currentUniqueId is multi-dimensional, then it belongs to a list of Strings
        If IsMultiDimensionalKey(currentUniqueId) Then
            currentKey = currentKey.Pluralize()
            ' Attempt to get the list of Strings
            Dim list As List(Of String) = hash(currentKey)
            If list Is Nothing Then
                ' Create the list and add it to the parent Hash using the key
                list = New List(Of String)
                hash.Add(currentKey, list)
            End If
            ' Add the value to the list of Strings
            list.Add(value)
        Else
            ' Add the key and value to the Hash
            hash.Add(currentKey, value)
        End If
    End Sub

    Private Shared Function PluralizeKeys(collectionKeys As IEnumerable(Of String)) As List(Of String)
        Return (From key In collectionKeys Select key.Pluralize()).ToList()
    End Function
End Class