﻿Imports System.Runtime.CompilerServices

Module Extensions
    <Extension> Public Function Contains(haystack As IEnumerable(Of String), needle As String, comparison As StringComparison) As Boolean
        Return haystack.Any(Function(s) s.Equals(needle, comparison))
    End Function
End Module
