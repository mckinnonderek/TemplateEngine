﻿Imports DotLiquid

''' <summary>
''' The base Generator from which all other Generators inherit
''' </summary>
''' <remarks></remarks>
Public Class Generator

    Private _output As String

    Protected Property Template As String
    Protected Property Values As Hash

    ''' <summary>
    ''' Gets the output result of the generation process
    ''' </summary>
    Public ReadOnly Property Output As String
        Get
            Return _output
        End Get
    End Property

    ''' <summary>
    ''' Constructs a new Generator object using the template and values
    ''' </summary>
    ''' <param name="template">The raw template text</param>
    ''' <param name="values">The keys and values to populate the template with</param>
    ''' <remarks></remarks>
    Public Sub New(template As String, values As IDictionary(Of String, Object))
        Me.New()
        Me.Template = template
        Me.Values = Hash.FromDictionary(values)
    End Sub

    Protected Sub New()
        _output = String.Empty
    End Sub

    ''' <summary>
    ''' Renders the output of the template and values
    ''' </summary>
    Public Overridable Function Build() As String
        Dim liquidTemplate = DotLiquid.Template.Parse(Template)
        _output = liquidTemplate.Render(Values)
        Return _output
    End Function

End Class
