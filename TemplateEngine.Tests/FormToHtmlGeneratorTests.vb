﻿Imports System.IO
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Collections.Specialized

<TestClass()> Public Class FormToHtmlGeneratorTests

    <TestMethod()> Public Sub TestFormToHtml()
        Dim formValues = New NameValueCollection
        formValues.Add("ct001$test$txtTitle", "Test Template")
        formValues.Add("ct001$test$txtName", "Derek")

        formValues.Add("ct001$test$txtColour1", "Red")
        formValues.Add("ct001$test$txtColour2", "Green")
        formValues.Add("ct001$test$txtColour3", "Blue")

        formValues.Add("ct001$test$txtCountry1Name", "United States")
        formValues.Add("ct001$test$txtCountry1City1", "New York")
        formValues.Add("ct001$test$txtCountry1City2", "Los Angeles")
        formValues.Add("ct001$test$txtCountry1City3", "Boston")

        formValues.Add("ct001$test$txtCountry2Name", "Canada")
        formValues.Add("ct001$test$txtCountry2City1", "Vancouver")
        formValues.Add("ct001$test$txtCountry2City2", "Toronto")
        formValues.Add("ct001$test$txtCountry2City3", "Montreal")
        formValues.Add("ct001$test$txtCountry2City4", "Ottawa")

        formValues.Add("ct001$test$txtAddress1Street", "300 Slater Street")
        formValues.Add("ct001$test$txtAddress1City", "Ottawa")
        formValues.Add("ct001$test$txtAddress1Province", "Ontario")
        formValues.Add("ct001$test$txtAddress1PostalCode", "K1P 6A6")

        formValues.Add("ct001$test$txtAddress2Street", "200, promenade du Portage")
        formValues.Add("ct001$test$txtAddress2City", "Gatineau")
        formValues.Add("ct001$test$txtAddress2Province", "Qu&eecute;bec")
        formValues.Add("ct001$test$txtAddress2PostalCode", "J8X 4B7")

        Dim projectDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName
        Dim templatePath = projectDir + "\TestTemplate.html"
        Dim generator = New FormToHtmlGenerator(templatePath, formValues)
        generator.Build()
        Console.Write(generator.Output)
    End Sub

End Class