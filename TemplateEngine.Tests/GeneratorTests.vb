﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class GeneratorTests

    <TestMethod()> Public Sub TestGeneratorOutputsCorrectData()
        Const template As String = "Hello {{ name }}"
        Dim values = New Dictionary(Of String, Object)
        values.Add("name", "Derek")
        Dim generator = New Generator(template, values)
        generator.Build()
        Assert.AreEqual("Hello Derek", generator.Output)
    End Sub

    <TestMethod()> Public Sub TestGeneratorDoesNotOutputIncorrectData()
        Const template As String = "Hello {{ name }}"
        Dim values = New Dictionary(Of String, Object)
        values.Add("test", "Derek")
        Dim generator = New Generator(template, values)
        generator.Build()
        Assert.AreNotEqual("Hello Derek", generator.Output)
    End Sub

End Class